<?php
header('Content-Type: text/html; charset=UTF-8');
define ('dblogin',TRUE);
include 'dblogin.php';
include 'scripts.php';
isAdmin($db);

$id = $db->quote($_GET['id']);
$request = "SELECT * FROM form6 where id=$id";
$result = $db->prepare($request);
$result->execute();
$data = $result->fetch(PDO::FETCH_ASSOC);
if($data==false){
    header('Location:admin.php');
    exit();
}
session_start();

$_SESSION['login'] = $data['login'];
$_SESSION['id'] =strip_tags($id);

setcookie('change_c','1');
setcookie('admin', '1');

header('Location: index.php');
?>
