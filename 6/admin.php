<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
if (empty($_SERVER['PHP_AUTH_USER']) ||
empty($_SERVER['PHP_AUTH_PW']) ||
$_SERVER['PHP_AUTH_USER'] != 'admin' ||
md5($_SERVER['PHP_AUTH_PW']) != md5('123')) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="666"');
  print('<h1>401 Требуется авторизация</h1>
    <br>
    <a href="index.php">Вернуться на начальную страницу</a>
    ');
  
  exit();
}
$user = 'u20936';
$pass = '1532365';
$db = new PDO('mysql:host=localhost;dbname=u20936', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$request = "SELECT * from admin";
$result = $db->prepare($request);
$result->execute();

print('<div class="block">Вы успешно авторизовались и видите защищенные паролем данные.</div>');

// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********
$request = "SELECT * from form6";
$result = $db ->prepare($request);
$result->execute();
?>

<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" media="all" href="style.css"/>
</head>
<body >
<div class="block">
<h2>Страница Администратора</h2> <br><br><br>
<form action="delete.php" method="post" accept-charset="UTF-8">
    <label>
    Удалить данные по ID:
        <input  name="deleteid">
    </label>
    <input type="submit">
</form>
<form action="change.php" method="post" accept-charset="UTF-8">
    <label>
    Изменить данные по ID:
        <input  name="changeid">
    </label>
    <input type="submit">
    
</form>
</div>

<?php

$request = "SELECT * from form6 INNER JOIN superpower6 on form6.id=superpower6.id";
$result = $db ->prepare($request);
$result->execute();
print '<div class="block"><br><table>';
print '
<tr><th>ID</th>
<th>ФИО</th>
<th>E-Mail</th>
<th>Дата рождения</th>
<th>Пол</th>
<th>Количество конечностей</th>
<th>Биография</th>
<th>Логин</th>
<th>Пароль</th>
<th>godmode</th>
<th>wallhack</th>
<th>noclip</th></tr>';
while($data = $result->fetch()){
    print '<tr><td>';
    print $data['id'];
    print '</td><td>';
    print $data['fio'];
    print '</td><td>';
    print $data['email'];
    print '</td><td>';
    print $data['bdate'];
    print '</td><td>';
    print $data['sex'];
    print '</td><td>';
    print $data['limb'];
    print '</td><td>';
    print $data['bio'];
    print '</td><td>';
    print $data['login'];
    print '</td><td>';
    print $data['pass'];
    print '</td><td>';
    print $data['godmode'];
    print '</td><td>';
    print $data['wallhack'];
    print '</td><td>';
    print $data['noclip'];
    print '</td></tr>';
}
print '</table>';

$request = "SELECT COUNT(godmode) FROM superpower6 WHERE godmode=1 GROUP BY godmode ";
$result = $db ->prepare($request);
$result->execute();
$data_im = $result->fetch()[0];
$request = "SELECT COUNT(wallhack) FROM superpower6 WHERE wallhack=1 GROUP BY wallhack ";
$result = $db ->prepare($request);
$result->execute();
$data_ph = $result->fetch()[0];
$request = "SELECT COUNT(noclip) FROM superpower6 WHERE noclip=1 GROUP BY noclip ";
$result = $db ->prepare($request);
$result->execute();
$data_lv = $result->fetch()[0];
print '<h2>Статистика по сверхспособностям</h2>';
print '<table>';
print '
<tr><th>godmode</th>
<th>wallhack</th>
<th>noclip</th></tr>';
print '<tr><td>';
print $data_im;
print '</td><td>';
print $data_ph;
print '</td><td>';
print $data_lv;
print '</td></tr>';
print '</table>';

print ' <a href="index.php">на главную</a></div>';

?>

</body>
</html>
